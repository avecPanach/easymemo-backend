const http = require('http');
const app = require('./app');

const server = http.createServer(app);

console.log('Server listened on port 3000')
server.listen(3000);
