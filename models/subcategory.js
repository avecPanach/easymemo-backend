const mongoose = require('mongoose');

const SubcategorySchema = mongoose.Schema({
    name: { type: String, required: true },
    description: { type: String, required: true },
    items: { type: Array, required: false }
});

module.exports = mongoose.model('Subcategory', SubcategorySchema);
