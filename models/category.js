const mongoose = require('mongoose');

const categorySchema = mongoose.Schema({
    name: { type: String },
    description: { type: String },
    subCategories: { type: Array }
});

module.exports = mongoose.model('Category', categorySchema);
