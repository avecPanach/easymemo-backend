const express = require('express')
const categoryCtrl = require('../controller/category.js')

const router = express.Router();

router.post('/', categoryCtrl.createCategory)
router.get('/', categoryCtrl.getAllCategories)

module.exports = router;
