const Category = require('../models/category');

exports.createCategory = (req, res, next) => {
    const category = new Category({
        // FIXME : an error append when i use api with insomnia
        name: req.body.description,
        description: req.body.description
    });
    category.save().then(
      () => {
        res.status(201).json({
            message: 'Category saved successfully!'
        });
      }
    ).catch(
      (error) => {
        res.status(400).json({
            error: error
        });
      }
    );
};

exports.getAllCategories = (req, res, next) => {
    Category.find().then(
        (categories) => {
            res.status(200).json(categories);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};
