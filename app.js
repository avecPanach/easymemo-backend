const app = require('express')();
const mongoose = require('mongoose');
const categoryRoutes = require('./routes/category');
// const subcategoryRoutes = require('./routes/subcategory');
// const itemRoutes = require('./routes/item');

mongoose.connect("mongodb+srv://panach:panach@cluster0.uengz.mongodb.net/EasyMemo?retryWrites=true&w=majority", 
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)
.then(() => console.log('Connexion à MongoDB réussie !'))
.catch(() => console.log('Connexion à MongoDB échouée !'));

app.use('/category', categoryRoutes);
// app.use('/subcategory', subcategoryRoutes);
// app.use('/item', itemRoutes);

module.exports = app;
